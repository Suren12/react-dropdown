export default [
  {
    "id": "123",
    "date": "2018-10-08T15:03:23.000Z",
    "title": "test title1",
    "text": "test text1",
    "comments": [
      {
        "id": "qwerqwr",
        "user": "user 1",
        "text": "text1",
      },
      {
        "id": "qwerqwr2",
        "user": "user 2",
        "text": "text2",
      },
      {
        "id": "qwerqwr3",
        "user": "user 3",
        "text": "text3",
      },
      {
        "id": "qwerqwr4",
        "user": "user 4",
        "text": "text4",
      },
      {
        "id": "qwerqwr5",
        "user": "user 5",
        "text": "text5",
      }
    ]
  },
  {
    "id": "1234",
    "date": "2017-10-08T15:03:23.000Z",
    "title": "test title2",
    "text": "test text2",
  },
  {
    "id": "12345",
    "date": "2017-12-08T15:03:23.000Z",
    "title": "test title3",
    "text": "test text3",
  }
]
